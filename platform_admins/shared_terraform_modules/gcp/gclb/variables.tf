variable "project_id" {
  type = string
}

variable "env" {
  type = string
}

variable "network_name" {
  type = string
}
