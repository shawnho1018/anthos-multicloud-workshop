variable "project_id" {
  type = string
}

variable "gke_name" {
  type = string
}

variable "gke_location" {
  type = string
}
