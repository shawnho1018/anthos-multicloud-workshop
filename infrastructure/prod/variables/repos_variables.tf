variable "platform_admins" { default = "platform-admins" }
variable "acm" { default = "config" }
variable "sharedcd" { default = "shared-cd" }

variable "online_boutique_group" { default = "online-boutique" }
variable "online_boutique_project" { default = "online-boutique" }

variable "bank_of_anthos_group" { default = "bank-of-anthos" }
variable "bank_of_anthos_project" { default = "bank-of-anthos" }
