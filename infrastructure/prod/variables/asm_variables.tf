variable "gke_net" {
  default = "prod-gcp-vpc"
}

variable "asm_version" {
  default = "1.6.8-asm.9"
}

variable "env" {
  default = "prod"
}
